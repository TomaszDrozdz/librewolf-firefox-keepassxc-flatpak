[[_TOC_]]


LibreWolf - FireFox - KeePassXC - Flatpak  
=========================================

So I have LibreWolf web browser and KeePassXC password manager both installed with Flatpak  
and want them to cooperate.  

So far (as of 24-11-2022) it does not work out of the box.  
There are many questions about and solutions for this impossibility.  
None of them worked for me.  
But I was able to make them cooperate  
and I share what I did so perhaps someone of You will find it useful also.  

I am not technically backgrounded enough to be in position to teach You about stuff / how it works / why it does not work.  
Here I rather provide how to / copy paste solution.  

If you want to know more about this issue  
You can refer e.g. to  
[https://discourse.flathub.org/t/how-to-run-firefox-and-keepassxc-in-a-flatpak-and-get-the-keepassxc-browser-add-on-to-work/437](https://discourse.flathub.org/t/how-to-run-firefox-and-keepassxc-in-a-flatpak-and-get-the-keepassxc-browser-add-on-to-work/437),  
[https://discussion.fedoraproject.org/t/how-to-run-firefox-and-keepassxc-in-a-flatpak-and-get-the-keepassxc-browser-add-on-to-work/19452](https://discussion.fedoraproject.org/t/how-to-run-firefox-and-keepassxc-in-a-flatpak-and-get-the-keepassxc-browser-add-on-to-work/19452),  
or [startpage](startpage.com) for it for more insight.  

> **Note**  
>
> Here for LibreWolf I use:  
> \- ".librewolf"  
> \- "io.gitlab.librewolf-community"  
>
> but for FireFox You should use:  
> \- ".mozilla"  
> \- "org.mozilla.firefox".  


# 1) Create native-messaging-hosts dir.  

`mkdir -p ~/.librewolf/native-messaging-hosts`  


# 2) Prepare "KeePassXC proxy".  

> **Note**  
> Prefer 2-B) over 2-A).  
> 
> \`flatpak-spawn --host ...\`  runs app in "unsandboxed mode".  
> 
> Giving KeePassXC org.freedesktop.Flatpak Session Bus Talk permision allows it to start any commands on the host.  
> 
> That gets out of line flatpak "security/ sandboxng" idea.  

## 2-A) With shell script around flatpak-spawn.  

`cd ~/.librewolf/native-messaging-hosts/`  

Create file  
`nvim org.keepassxc.KeePassXC.proxy`  

Give it content
```
#!/bin/sh
flatpak-spawn --host /usr/bin/flatpak run --command=keepassxc-proxy org.keepassxc.KeePassXC "$@"
```

and permissions  
`chmod a+rx org.keepassxc.KeePassXC.proxy`  

## 2-B) With Rust keepassxc proxy binary.

[keepassxc-proxy-rust](https://github.com/varjolintu/keepassxc-proxy-rust)  

> **Note**  
> You need to have Rust installed.  
    
Download it  
`mkdir -p /tmp/kpxcp`  
`cd /tmp/kpxcp`  
`wget https://github.com/varjolintu/keepassxc-proxy-rust/archive/refs/heads/master.zip`  

unpack it  
`unzip master.zip`  
`cd keepassxc-proxy-rust-master/`  

compile it (with static library)  
`RUSTFLAGS='-C link-arg=-s' cargo build --release --target x86_64-unknown-linux-musl`  

and copy it  
`cp target/x86_64-unknown-linux-musl/release/keepassxc-proxy ~/.librewolf/native-messaging-hosts/`  


# 3) Instruct FireFox to run this "KeePassXC proxy".

`cd ~/.librewolf/native-messaging-hosts/`

Create file  
`nvim org.keepassxc.keepassxc_browser.json`

and give it content  

```
{
    "allowed_extensions": [
        "keepassxc-browser@keepassxc.org"
    ],
    "description": "KeePassXC integration with native messaging support",
    "name": "org.keepassxc.keepassxc_browser",
    "path": "/home/REPLACE_WITH_USERNAME/.librewolf/native-messaging-hosts/keepassxc-proxy",
    "type": "stdio"
}
```

Of course for 2-A)

```
    "path": "/home/REPLACE_WITH_USERNAME/.librewolf/native-messaging-hosts/org.keepassxc.KeePassXC.proxy",
```


# 4) Configure LibreWolf and KeePassXC Flatpak permissions.

> **Note**  
> Here I make use of Flatseal  
> but You can equaly configure it from cmd with  
> `flatpak override --user ...`  
  
Install Flatseal  
`flatpak install flathub com.github.tchx84.Flatseal`  

Open it and configure  

```
org.keepassxc.KeePassXC:
    Filesystem:
        add: ~/.librewolf/native-messaging-hosts:ro

io.gitlab.librewolf-community
    Filesystem:
        add: ~/.librewolf/native-messaging-hosts:ro
        add: xdg-run/app/org.keepassxc.KeePassXC:ro
```

> **Note**  
> Give ro (read only) permissions to KeePassXC for  
> \`\~/.librewolf/native-messaging-hosts\`  
> It will prevent it from overwriting  
> \`org.keepassxc.keepassxc_browser.json'
> file with  
> ```
>     "path": "whatever KeePassXC thinks is correct",
> ```
> Also you will see  
> \`Could not save the native messaging script file for custom.\`  
> after launching - ignore it.  

Also for 2-A)  

```
org.keepassxc.KeePassXC:
    Session Bus:
        Talks:
            add: org.freedesktop.Flatpak
```

# 5) Configure KeePassXC.

```
Tools → Settings → Browser Integration
```

- In General tab  
    - make sure Firefox is checked.
- In Advanced tab  
    - check "Use a custom browser configuration location",    
    - select Firefox as "Browser type"  
    - and provide "~/.librewolf/native-messaging-hosts" as "Config location".
